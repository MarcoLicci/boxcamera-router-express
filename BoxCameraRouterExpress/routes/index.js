﻿'use strict';
var express = require('express');
var router = express.Router();

var httpProxy = require('http-proxy'),
    proxy = httpProxy.createProxyServer({
        ignorePath: true
    });

var redis = require("redis"),
    redisClient = redis.createClient({host: "192.168.1.100"});

var url = require('url');

redisClient.on("error", function (err) {
    console.log("Error " + err);
});

/* GET home page. */
router.get('/', function (req, res) {
    res.render('index', { title: 'Express' });
});

router.all('/users/*', function (req, res, next) {
    var reqUrl = url.parse(req.path);
    var splittenReqUrl = reqUrl.pathname.split('/');
    var user = splittenReqUrl[2];

    redisClient.get(user, function (err, reply) {
        console.log(reply);
        console.log(err)
        if (!err && reply) {
            console.log("if");
            proxy.web(req, res, {
                target: 'http://' + reply + ':8765/' + splittenReqUrl.slice(3).join('/')
            });
        } else {
            next();
        }
        
    });
});


module.exports = router;
